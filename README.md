# The Simplest CI-CD Project
[![pipeline status](https://gitlab.com/gitlab-org/security-products/codequality/badges/master/pipeline.svg)](https://gitlab.com/jnickgo/the-simplest-ci-cd/commits/master)

A very simple example project that builds and runs tests.