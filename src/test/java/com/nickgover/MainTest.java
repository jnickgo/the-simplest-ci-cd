package com.nickgover;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Test facade to exercise build testing.
 */
public class MainTest {

    @Test
    public void mainMethodTestOne() {

        assertEquals(1, 1);
        System.out.println("Test 1 ran.");
    }

    @Test
    public void mainMethodTestTwo() {
        assertEquals(2, 2);
        System.out.println("Test 2 ran.");
    }

    @Test
    public void mainMethodTestThree() {
        assertEquals("Test", "Test");
        System.out.println("Test 3 ran.");
    }

    @Test
    public void mainMethodTestFour() {
        assertEquals(200, 200);
        System.out.println("Test 4 ran.");
    }

    @Test
    public void mainMethodTestFive() {
        assertEquals(123, 123);
        System.out.println("Test 5 ran.");
    }
}
